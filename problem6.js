// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
function avgSalariesByCountry(data) {
    if (!Array.isArray(data)) {
      return `Data must be an Array !`;
    }
    if (data.length === 0) {
      return `Data is empty !`;
    }
  
    const salarySumByCountry = [];
    const countOfCountry = [];
    for (let index = 0; index < data.length; index++) {
      const salaryString = data[index].salary.substring(1);
      const salaryInNum = parseFloat(salaryString);
  
      if (!isNaN(salaryInNum) && data[index].location) {
        if (salarySumByCountry[data[index].location]) {
          salarySumByCountry[data[index].location] += salaryInNum;
          countOfCountry[data[index].location]++;
        } else {
          salarySumByCountry[data[index].location] = salaryInNum;
          countOfCountry[data[index].location] = 1;
        }
      } else {
        console.log("salary is not a number or country missing!");
      }
    }

    const avgSalaryByCountry = [];
    for( const country in salarySumByCountry) {
        const totalSumByCountry = salarySumByCountry[country];
        const totalCountOfCountry = countOfCountry[country];
        const avgSalary = totalSumByCountry/totalCountOfCountry;
        avgSalaryByCountry[country] = avgSalary;
    }

    return avgSalaryByCountry;
  }
  module.exports = avgSalariesByCountry;
  