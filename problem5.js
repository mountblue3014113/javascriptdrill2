// 4. Find the sum of all salaries.
function sumOfSalariesByCountry(data) {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  let salarySumByCountry = [];
  for (let index = 0; index < data.length; index++) {
    const salaryString = data[index].salary.substring(1);
    const salaryInNum = parseFloat(salaryString);

    if (!isNaN(salaryInNum) && data[index].location) {
      if (salarySumByCountry[data[index].location]) {
        salarySumByCountry[data[index].location] += salaryInNum;
      } else {
        salarySumByCountry[data[index].location] = salaryInNum;
      }
    } else {
      console.log("salary is not a number or country missing!");
    }
  }
  return salarySumByCountry;
}
module.exports = sumOfSalariesByCountry;
