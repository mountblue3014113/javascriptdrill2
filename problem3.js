// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
function salaryAfterCorrection(data) {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  for (let index = 0; index < data.length; index++) {
    const salaryString = data[index].salary.substring(1);
    const salaryInNum = parseFloat(salaryString);

    if (!isNaN(salaryInNum)) {
      const correctedSalary = salaryInNum * 10000;
      data[index].corrected_salary = correctedSalary;
    } else {
      data[index].corrected_salary = null;
      console.log("Not able to correct salary with given factor!");
    }
  }
  return data;
}
module.exports = salaryAfterCorrection;
