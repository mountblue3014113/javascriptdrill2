// 2. Convert all the salary values into proper numbers instead of strings.
function convertSalaryValuesInToNumber(data) {
  if (!Array.isArray(data)) {
    return `Data must be an Array !`;
  }
  if (data.length === 0) {
    return `Data is empty !`;
  }

  for (let index = 0; index < data.length; index++) {
    var salaryString = data[index].salary.substring(1);
    var salaryInNum = parseFloat(salaryString);

    if (!isNaN(salaryInNum)) {
      data[index].salary = salaryInNum;
    } else {
      data[index].salary = null;
      console.log("Not able to convert salary to pure num !");
    }
  }
  return data;
}
module.exports = convertSalaryValuesInToNumber;
