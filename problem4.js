// 4. Find the sum of all salaries.
function sumOfAllSalaries(data) {
    if (!Array.isArray(data)) {
      return `Data must be an Array !`;
    }
    if (data.length === 0) {
      return `Data is empty !`;
    }
  
    let salarySum = 0;
    for (let index = 0; index < data.length; index++) {
      const salaryString = data[index].salary.substring(1);
      const salaryInNum = parseFloat(salaryString);
  
      if (!isNaN(salaryInNum)) {
        salarySum += salaryInNum;
      }else {
        console.log("salary is not a number !");
      }
    }
    //salarySum = Math.round(salarySum);
    return salarySum;
  }
  module.exports = sumOfAllSalaries;
  